# Implementation of the Schulze method in Rust

[![crates.io](https://meritbadge.herokuapp.com/schulze)](https://crates.io/crates/schulze)

This is an implementation of the [Schulze method](https://en.wikipedia.org/wiki/Schulze_method)
in the [Rust Programming language](https://en.wikipedia.org/wiki/Rust_(programming_language)).
Be warned, it is still at an **early alpha stage**.
